package andreluiznsilva.ascential.impl;

import andreluiznsilva.ascential.CustomNumberEntity;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String[] args) {

        NumberFinderImpl impl = new NumberFinderImpl();

        List<CustomNumberEntity> values = impl.readFromFile("src/main/resources/example.json");

        long start = System.currentTimeMillis();

        boolean found = impl.contains(100, values);

        long end = System.currentTimeMillis();

        long seconds = TimeUnit.SECONDS.convert(end - start, TimeUnit.MILLISECONDS);

        System.out.println(String.format("found=%s in %s seconds", found, seconds));

    }

}
