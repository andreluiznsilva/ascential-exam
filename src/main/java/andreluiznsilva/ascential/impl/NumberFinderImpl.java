package andreluiznsilva.ascential.impl;

import andreluiznsilva.ascential.CustomNumberEntity;
import andreluiznsilva.ascential.FastestComparator;
import andreluiznsilva.ascential.NumberFinder;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class NumberFinderImpl implements NumberFinder {

    private FastestComparator comparator = new FastestComparator();

    // not sure if it was expected in the test
    // as there is no other way to instantiate the object and the class cannot be extended
    // Should I change the original class?
    public static CustomNumberEntity newInstance(String number) {
        try {
            Constructor<?> constructor = CustomNumberEntity.class.getDeclaredConstructors()[1];
            constructor.setAccessible(true);
            return (CustomNumberEntity) constructor.newInstance(number);
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean contains(int valueToFind, List<CustomNumberEntity> list) {

        Objects.requireNonNull(list, "list cannot be null");

        return list.parallelStream().anyMatch(item -> comparator.compare(valueToFind, item) == 0);

    }

    @Override
    public List<CustomNumberEntity> readFromFile(String filePath) {

        Objects.requireNonNull(filePath, "filePath cannot be null");

        Path file = Paths.get(filePath);

        if (!Files.exists(file)) throw new IllegalArgumentException("filePath '" + filePath + "' do not exist");
        if (Files.isDirectory(file)) throw new IllegalArgumentException("filePath '" + filePath + "' is a directory");

        List<CustomNumberEntity> results = new ArrayList<>();

        String json = readFile(file);

        Matcher matcher = Pattern.compile("\"number\": \"(\\d+)\"").matcher(json);

        while (matcher.find()) {

            String number = matcher.group(1);

            CustomNumberEntity instance = newInstance(number);

            results.add(instance);

        }

        return results;

    }

    private String readFile(Path file) {
        try (Stream<String> lines = Files.lines(file)) {
            return lines.collect(Collectors.joining("\n"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
