package andreluiznsilva.ascential.impl;

import andreluiznsilva.ascential.CustomNumberEntity;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class NumberFinderImplTest {

    private NumberFinderImpl finder;

    @Before
    public void setUp() {
        finder = new NumberFinderImpl();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void readFromFileWithNullPath() {
        try {
            finder.readFromFile(null);
            fail("Expecting a exception");
        } catch (NullPointerException e) {
            assertEquals("filePath cannot be null", e.getMessage());
        }
    }

    @Test
    public void readFromFileWithInvalidPath() {
        try {
            finder.readFromFile("/nowhere");
            fail("Expecting a exception");
        } catch (IllegalArgumentException e) {
            assertEquals("filePath '/nowhere' do not exist", e.getMessage());
        }
    }

    @Test
    public void readFromFileWithDirectory() {
        try {
            finder.readFromFile(".");
            fail("Expecting a exception");
        } catch (IllegalArgumentException e) {
            assertEquals("filePath '.' is a directory", e.getMessage());
        }
    }

    @Test
    public void readFromFileWithEmptyJson() {
        List<CustomNumberEntity> results = finder.readFromFile("src/test/resources/empty.json");
        assertEquals(0, results.size());
    }

    @Test
    public void readFromFileWithNoNumberPropertyJson() {
        List<CustomNumberEntity> results = finder.readFromFile("src/test/resources/noNumber.json");
        assertEquals(0, results.size());
    }

    @Test
    public void readFromFileWithNullNumberJson() {
        List<CustomNumberEntity> results = finder.readFromFile("src/test/resources/nullNumber.json");
        assertEquals(0, results.size());
    }

    @Test
    public void readFromFileWithInvalidNumberJson() {
        List<CustomNumberEntity> results = finder.readFromFile("src/test/resources/invalidNumber.json");
        assertEquals(0, results.size());
    }

    @Test
    public void readFromFileWithOneNumberJson() {
        List<CustomNumberEntity> results = finder.readFromFile("src/test/resources/oneNumber.json");
        assertEquals(1, results.size());
        assertEquals("67", results.get(0).getNumber());
    }

    @Test
    public void readFromFileWithExampleJson() {
        List<CustomNumberEntity> results = finder.readFromFile("src/main/resources/example.json");
        assertEquals(6, results.size());
        assertEquals("67,45,45,12,100,3", results.stream().map(i -> i.getNumber()).collect(Collectors.joining(",")));
    }

    @Test
    public void containsWithNullList() {
        try {
            finder.contains(0, null);
            fail("Expecting a exception");
        } catch (NullPointerException e) {
            assertEquals("list cannot be null", e.getMessage());
        }
    }

    @Test
    public void containsWithEmptyList() {

        List<CustomNumberEntity> list = generateList();

        boolean result = finder.contains(0, list);

        assertFalse(result);

    }


    @Test
    public void containsWithOneItemAndValueNotOnTheList() {

        List<CustomNumberEntity> list = generateList(1);

        boolean result = finder.contains(0, list);

        assertFalse(result);

    }

    @Test
    public void containsWithOneItemAndValueInTheList() {

        List<CustomNumberEntity> list = generateList(1);

        boolean result = finder.contains(1, list);

        assertTrue(result);

    }

    @Test
    public void containsWithMultipleItensAndValueNotOnTheList() {

        List<CustomNumberEntity> list = generateList(1, 2, 3, 4, 5, 6, 7, 8, 9);

        boolean result = finder.contains(0, list);

        assertFalse(result);

    }

    @Test
    public void containsWithMultipleItensAndValueInTheList() {

        List<CustomNumberEntity> list = generateList(1, 2, 3, 4, 5, 6, 7, 8, 9);

        boolean result = finder.contains(6, list);

        assertTrue(result);

    }

    private List<CustomNumberEntity> generateList(int... values) {
        List<CustomNumberEntity> list = new ArrayList<>();
        for (int value : values) {
            list.add(NumberFinderImpl.newInstance(String.valueOf(value)));
        }
        return list;
    }

}
