# Ascential techtest #

## Assumptions ##

- Java 8 or greater
- There is no external dependency of any library

### Build ###

```
  mvn clean install
```
### Test ###

```
  mvn test
```

### Run Simulation ###

```
 java -jar target/ascential.jar
```

## Author

* **Andre Nascimento** 
  - [Email](mailto:andreluiznsilva@gmail.com)
  - [Linkedin](http://li.andreluiznsilva.tk)
  - [Github](http://github.com/andreluiznsilva)  
  - [Curriculum Vitae](http://cv.andreluiznsilva.tk)